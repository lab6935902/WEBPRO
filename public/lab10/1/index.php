<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
</head>
<body data-bs-theme="dark" class="container">
    <?php
        class MyDB extends SQLite3 {
            function __construct() {
               $this->open('customers.db');
            }
         }
      
         // 2. Open Database 
         $db = new MyDB();
         if(!$db) {
            echo $db->lastErrorMsg();
         } else {
            //echo "Opened database successfully<br>";
         }
        
        $sql ="SELECT * FROM customers";
        $ret = $db->query($sql);   
        echo "<table class='table table-dark table-striped'>";
        echo "
        <tr>
         <th>ID</th>
         <th>Name</th>
         <th>Address</th>
         <th>Phone</th>
         <th>Email</th>
        </tr>
        ";
        while($row = $ret->fetchArray(SQLITE3_ASSOC) ) {
            echo "<tr>";
            echo "<td> ". $row['CustomerId'] . "</td>";
            echo "<td> ". $row['FirstName']." ".$row["LastName"] . "</td>";
            echo "<td> ". $row['Address'] . "</td>";
            echo "<td> ". $row['Phone'] . "</td>";
            echo "<td> ". $row['Email'] . "</td>";
            echo "</tr>";
        }
        echo "</table>";
        //echo "Operation done successfully<br>";
        $sql = "SELECT * FROM customers ORDER BY ROWID DESC LIMIT 1";
        $last_id = $db->query($sql)->fetchArray(SQLITE3_ASSOC)["CustomerId"];
    
        echo "
        <form action='delete.php' method='post'>
            <input style='display:none;' name='id' id='id' value='".$last_id."'>
            <button type='submit' class=\"btn btn-light mb-5\">Delete last row</button>
        </form>
        ";
        $db->close();
    ?>
    
</body>
</html>