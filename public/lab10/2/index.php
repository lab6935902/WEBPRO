<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
</head>
<body data-bs-theme="dark" class="container">
    <h1 class="mb-5 alert alert-dark mt-3">Quizz</h1>
    <div class="alert alert-dark">
    <?php
        session_start();
        if(key_exists("qid",$_GET) && key_exists("ans",$_GET)){
            $_SESSION["Q".$_GET["qid"]] = array(
                "answed"=>$_GET["ans"],
                "QID"=>$_GET["qid"]
            );
        }
        class MyDB extends SQLite3 {
            function __construct() {
               $this->open('questions.db');
            }
         }
      
         // 2. Open Database 
         $db = new MyDB();
         if(!$db) {
            echo $db->lastErrorMsg();
         } else {
            //echo "Opened database successfully<br>";
         }

        $offset = 0;
        if(key_exists("offset",$_GET)){
            
          $offset = $_GET["offset"];  
        }else{
            session_destroy();
        }
        $sql ="SELECT * FROM questions LIMIT 1 OFFSET ".$offset.";";
        $ret = $db->query($sql);   
        $row = $ret->fetchArray(SQLITE3_ASSOC);
        $sql = "SELECT COUNT(QID) FROM questions LIMIT 1 OFFSET 0;";
        $limitoff = $db->query($sql)->fetchArray(SQLITE3_ASSOC)["COUNT(QID)"];
        if($offset == $limitoff){
            echo "<form action='submit.php' method='post'>
            <h3>Submit Quizz</h3>
            <p>Wow You Finshed quiz</p>
            <p>Submit grading</p>
            <div class='d-flex'>
            <button class='btn btn-danger'>Submit</button>
        </div> </form>";
        }
        else{
        echo "
            <form>
                <h3>".$row["Stem"]."</h3>
                <input style='display:none;' name='qid' id='qid' value='".$row["QID"]."'>
                <input style='display:none;' name='offset' id='offset' value='".($offset+1)."'>
                <div class=\"form-check\">
                    <input class=\"form-check-input\" name='ans' id='alt1' value='A' type='radio'/>
                    <label class=\"form-check-label\" for='alt1'>".$row["Alt_A"]."</label>
                </div>
                <div class=\"form-check\">
                    <input class=\"form-check-input\" name='ans' id='alt2' value='B' type='radio'/>
                    <label class=\"form-check-label\" for='alt2'>".$row["Alt_B"]."</label>

                </div>
                <div class=\"form-check\">
                    <input class=\"form-check-input\" name='ans' id='alt3' value='C' type='radio'/>
                    <label class=\"form-check-label\" for='alt3'>".$row["Alt_C"]."</label>

                </div>
                <div class=\"form-check\">
                    <input class=\"form-check-input\" name='ans' id='alt4' value='D' type='radio'/>
                    <label class=\"form-check-label\" for='alt4'>".$row["Alt_D"]."</label>
                </div>
                <div class='d-flex'>
                    <button class='btn btn-success'>Next</button>
                </div>
            </form>";
        }
        $db->close();
    ?>
    </div>
</body>
</html>