
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
</head>
<body data-bs-theme="dark" class="container">
    <h1 class="mb-5 alert alert-dark mt-3">Graded</h1>
    <div class="alert alert-dark">
        <?php

        session_start();
        class MyDB extends SQLite3 {
            function __construct() {
            $this->open('questions.db');
            }
        }

        // 2. Open Database 
        $db = new MyDB();
        if(!$db) {
            echo $db->lastErrorMsg();
        } else {
            //echo "Opened database successfully<br>";
        }

        $offset = 0;
        if(key_exists("offset",$_GET)){
        $offset = $_GET["offset"];  
        }
        $sql ="SELECT * FROM questions ;";
        $ret = $db->query($sql);   
        $count = 0;
        while($row = $ret->fetchArray(SQLITE3_ASSOC) ) {
            if(key_exists("Q".$row["QID"],$_SESSION)){
                if(strcmp($_SESSION["Q".$row["QID"]]["answed"],$row["Correct"]) == 0 ){
                    echo "<p style='color:green;'>".$row["QID"]." Your ans (is correct) : ".$_SESSION["Q".$row["QID"]]["answed"]."</p>";
                    $count ++;
                }
                else{
                    echo "<p style='color:red;'>".$row["QID"]." Your ans : ".$_SESSION["Q".$row["QID"]]["answed"]." Correct is ".$row["Correct"]."</p>";
                }
            }else{
                echo "<p style='color:red;'>".$row["QID"]." You don't ans </p>";
            }
        }
        echo "<h4>You got score : ".$count."</h4>";
        session_destroy();
        ?>
        <form action='./'>
            <button style="width:100%;" class='btn btn-dark mt-5'>Attempt Again</button>        
        </form>
    </div>
</body>
</html>
