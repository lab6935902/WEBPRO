<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Employee Information</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/water.css@2/out/dark.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Mitr:wght@200;300;400;500;600;700&display=swap" rel="stylesheet">
    <style>
        * {
            font-family: "Mitr", sans-serif;
        }
    </style>
</head>

<body>
    <h1>Employee Information</h1>
    <form method="get" action="./index.php">
        <?php
        session_start();

        if (key_exists("clear", $_GET) || (key_exists("show", $_GET) && !key_exists("id", $_SESSION))) {
            session_destroy();
            echo '<label>Employee ID : </label>
            <input style="width:98%" name="id" id="id" type="number"/>
            <label>Firstname : </label>
            <input style="width:98%" name="firstname" id="firstname" />
            <label>Lastname : </label>
            <input style="width:98%" name="lastname" id="lastname" />
            <label>Address : </label>
            <textarea  name="address" id="address" rows="5"></textarea>
            <label>Email : </label>
            <input style="width:98%" name="email" id="email" />
            <label>Phone : </label>
            <input style="width:98%" name="phone" id="phone" />
            ';
        } elseif (key_exists("save", $_GET)) {
            $_SESSION["id"] = $_GET["id"];
            $_SESSION["firstname"] = $_GET["firstname"];
            $_SESSION["lastname"] = $_GET["lastname"];
            $_SESSION["address"] = $_GET["address"];
            $_SESSION["email"] = $_GET["email"];
            $_SESSION["phone"] = $_GET["phone"];
            echo '<label>Employee ID : </label>
            <input style="width:98%" name="id" id="id" type="number"/>
            <label>Firstname : </label>
            <input style="width:98%" name="firstname" id="firstname" />
            <label>Lastname : </label>
            <input style="width:98%" name="lastname" id="lastname" />
            <label>Address : </label>
            <textarea  name="address" id="address" rows="5"></textarea>
            <label>Email : </label>
            <input style="width:98%" name="email" id="email" />
            <label>Phone : </label>
            <input style="width:98%" name="phone" id="phone" />
            ';
        } elseif (key_exists("show", $_GET) && key_exists("id", $_SESSION)) {
            echo '<label>Employee ID : </label>
            <input style="width:98%" name="id" id="id" type="number" value="' . $_SESSION["id"] . '"/>
            <label>Firstname : </label>
            <input style="width:98%" name="firstname" id="firstname" value="' . $_SESSION["firstname"] . '"/>
            <label>Lastname : </label>
            <input style="width:98%" name="lastname" id="lastname" value="' . $_SESSION["lastname"] . '" />
            <label>Address : </label>
            <textarea  name="address" id="address" rows="5">' . $_SESSION["address"] . '</textarea>
            <label>Email : </label>
            <input style="width:98%" name="email" id="email" value="' . $_SESSION["email"] . '"/>
            <label>Phone : </label>
            <input style="width:98%" name="phone" id="phone" value="' . $_SESSION["phone"] . '"/>
            ';
        } else {
            class MyDb extends SQLite3
            {
                public function __construct()
                {
                    $this->open("customers.db");
                }
            }
            $db = new MyDb();
            if (!$db) {
                echo $db->lastErrorMsg();
            } else {
                echo "Pull data from database successfully</br></br>";
            }

            $res = $db->query("select * from customers");
            $row = $res->fetchArray(SQLITE3_ASSOC);

            $db->close();
            echo '<label>Employee ID : </label>
            <input style="width:98%" name="id" id="id" type="number" value="' . $row["CustomerId"] . '" />
            <label>Firstname : </label>
            <input style="width:98%" name="firstname" id="firstname" value="' . $row["FirstName"] . '" />
            <label>Lastname : </label>
            <input style="width:98%" name="lastname" id="lastname" value="' . $row["LastName"] . '" />
            <label>Address : </label>
            <textarea  name="address" id="address" rows="5">' . $row["Address"] . '</textarea>
            <label>Email : </label>
            <input style="width:98%" name="email" id="email" value="' . $row["Email"] . '" />
            <label>Phone : </label>
            <input style="width:98%" name="phone" id="phone" value="' . $row["Phone"] . '" />
            ';
        }
        ?>
        <input style="display:none" name="save" id="save" value="true" />
        <br />
        <button style="width:100%">Save Data</button>
    </form>
    <div style="display:flex">
        <form method="get" action="./">
            <input style="display:none" name="show" id="show" value="true" />
            <button>Show Data</button>
        </form>
        <form method="get" action="./">
            <input style="display:none" name="clear" id="clear" value="true" />
            <button>Clear Data</button>
        </form>
        <form method="get" action="./">
            <button>ReFetch</button>
        </form>
    </div>
</body>

</html>