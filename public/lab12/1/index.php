<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap demo</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
</head>
<?php
$res = file_get_contents("http://10.0.15.21/lab/lab12/restapis/10countries.json");
?>

<body class="container">
    <h1 class="alert alert-light">10 Countries</h1>
    <?php
    $resjson = json_decode($res,true);
    foreach($resjson as $val){
        $val = json_encode($val);
        $val = json_decode($val,true);

        echo '<div class="alert alert-light d-flex">
            <div style="width: 80%;">
                <h2>Name : '.$val["name"].'</h2>
                <hr/>
                <p>Capital : '.$val["capital"].'</p>
                <p>Population : '.$val["population"].'</p>
                <p>Region : '.$val["region"].'</p>
                <p>Localtion : '.join(" ",$val["latlng"]).' </p>
                <p>Border : '.join(" ",$val["borders"]).'</p>
            </div>
            <div class="alert alert-light ms-4" style="width: 40%;">
            <img width="100%" src="'.$val["flag"].'" alt="'.$val["name"].' flag" />
            </div>
        </div>';
    }
    ?>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>
</body>

</html>
