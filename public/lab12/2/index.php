<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Product Details</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/water.css@2/out/dark.css">
    <style>
        .flex {
            display: flex;
        }
    </style>
</head>
<?php
//(1-30)
$prodid = 1;
if (!isset($_GET["prodid"]) || $_GET["prodid"] < 1 || $_GET["prodid"] > 32) {
    $prodid = 1;
} else {
    $prodid = $_GET["prodid"];
}
$res = file_get_contents("http://10.0.15.21/lab/lab12/restapis/products.php?prodid=" . $prodid);
$resjson = json_decode($res, true);

?>

<body>
    <h1>Product Details</h1>
    <hr />
    <?php
    if (count($resjson) > 0) {
        $resjson = $resjson[0];
        echo '
            <div>
                <h2><b>ID : </b>' . $resjson["ProductID"] . '</h2>
                <p><b>Code : </b> ' . $resjson["ProductCode"] . '</p>
                <p><b>Name : </b> ' . $resjson["ProductName"] . ' </p>
                <p><b>Description : </b> ' . $resjson["Description"] . ' </p>
                <p><b>Price : </b> ' . $resjson["UnitPrice"] . ' </p>
            </div>';
    } else {
        echo "<blockquote>Not Found ProductID</blockquote>";
    }
    ?>
    <?php
    echo '<div class="flex">';
    if (($prodid - 1) <= 0) {
        echo '     <a disabled href="./?prodid=' . ($prodid - 1) . '"> <button disabled>Previous</button></a>';
    }else{
        echo '     <a href="./?prodid=' . ($prodid - 1) . '"> <button>Previous</button></a>';
    }
    echo '     <a href="./?prodid=' . ($prodid + 1) . '"> <button>Next</button></a>
          </div>';
    ?>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>
</body>

</html>
