<?php
$list = 10;
$res = file_get_contents("http://10.0.15.21/lab/lab12/restapis/products.php?list=" . $list);
$resjson = json_decode($res, true);

$dataPoints = array();

$dataPoints = array_map(function ($data) {
    return array("label" => $data["ProductCode"], "y" => $data["UnitPrice"]);
}, $resjson)


?>
<!DOCTYPE HTML>
<html>

<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">

    <script>
        window.onload = function() {

            var chart = new CanvasJS.Chart("chartContainer", {
                animationEnabled: true,
                exportEnabled: true,
                theme: "light2", // "light1", "light2", "dark1", "dark2"
                title: {
                    text: "Price of Products"
                },
                axisY: {
                    title: "Unit Price (in THB)",
                    includeZero: true
                },
                data: [{
                    type: "column", //change type to bar, line, area, pie, etc
                    indexLabel: "{y}", //Shows y value on all Data Points
                    indexLabelFontColor: "#fafafa",
                    indexLabelPlacement: "inside",
                    dataPoints: <?php echo json_encode($dataPoints, JSON_NUMERIC_CHECK); ?>
                }]
            });
            chart.render();

        }
    </script>
</head>

<body>
    <div class="alert alert-light m-2">
        <div id="chartContainer" style="height: 400px; width: 100%;"></div>
    </div>
    <script src="https://cdn.canvasjs.com/canvasjs.min.js"></script>
</body>

</html>
