let lang = "th"
let formInfo = {
    "th":{
        "keys":["name","lastname","country"],
        "name":"ชื่อ",
        "lastname":"นามสกุล",
        "country":"ประเทศ",
        "countryList":["เลือกประเทศ","กัมพูชา" ,"ไทย", "บรูไน" ,"พม่า", "ฟิลิปปินส์","มาเลเซีย", "ลาว", "เวียดนาม", "สิงคโปร์", "อินโดนีเซีย" ],
        "changeLanguage" :"เปลี่ยนภาษา"
    },
    "en":{
        "keys":["name","lastname","country"],
        "name":"name",
        "lastname":"lastname",
        "country":"country",
        "countryList":["Select country","Cambodia" ,"Thailand", "Brunei" ,"Myanmar", "Philippines","Malaysia", "Laos", "Vietnam", "Singapore", "Indonesia" ],
        "changeLanguage" :"changeLanguage"
    },
    
}

function clearElement(){
    let container = document.getElementById("container")
    container.firstChild.remove()
}

function changeTextLang(){
    let container = document.getElementById("container")


    let content = document.createElement("div")
   
    let country = document.createElement("select")
    country.setAttribute("class","form-select")
    for(let i of formInfo[lang].countryList){let option = document.createElement("option");option.appendChild(document.createTextNode(i));country.appendChild(option)}
    

    for(let i of formInfo[lang].keys){
        let label = document.createElement("label");label.setAttribute("class","btn btn-dark");label.appendChild(document.createTextNode(formInfo[lang][i]))

        let div = document.createElement("div");div.setAttribute("class","input-group mt-3")

        div.appendChild(label)
        if(i!="country"){let input = document.createElement("input");input.setAttribute("class","form-control");div.appendChild(input)
        }else{ div.appendChild(country)}
        content.appendChild(div)
    }

    let bt = document.createElement("button")
    bt.setAttribute("class","btn btn-dark mt-5")
    bt.appendChild(document.createTextNode(formInfo[lang].changeLanguage))
    bt.onclick = function (){lang = lang=="en" ? "th" :"en";changeTextLang()}
    content.appendChild(bt)
    container.replaceChild(content,container.firstChild)

}

changeTextLang()