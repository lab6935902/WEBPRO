async function initPage(){
    let resp = await fetch("./employees.json")
    let json = await resp.json()

    let result = document.getElementById("result")
    let ol = document.createElement("ol")
    for(let i of json){
     let li = document.createElement("li")
     let b = document.createElement("b")
     let p = document.createElement("span")
     b.appendChild(document.createTextNode(i.Name+" "+i.Lastname+" "))
     p.appendChild(document.createTextNode("("+i.Gender+") is a "+i.Position+", "+i.Address))
     li.appendChild(b)
     li.appendChild(p)

     ol.appendChild(li)
     
    }
    result.appendChild(ol)
}

initPage()