async function initPage() {
  let resp = await fetch("./person.json");
  let jsons = await resp.json();

  let result = document.getElementById("result");

  for (let json of jsons) {
    let p1 = document.createElement("p");
    let p2 = document.createElement("p");
    let p3 = document.createElement("p");
    let p4 = document.createElement("p");
    let div = document.createElement("div");

    let name = document.createElement("b");

    name.appendChild(
      document.createTextNode(json.firstName + " " + json.lastName)
    );

    p1.appendChild(name);
    p1.appendChild(
      document.createTextNode(
        ", " + json.gender.type + " " + json.age + " years old."
      )
    );
    p2.appendChild(
      document.createTextNode(
        json.address.streetAddress +
          " " +
          json.address.city +
          " " +
          json.address.state
      )
    );
    p3.appendChild(document.createTextNode(json.address.postalCode));

    for (let phoneNumber of json.phoneNumber) {
      let pp = document.createElement("p");
      pp.appendChild(
        document.createTextNode(phoneNumber.type + " : " + phoneNumber.number)
      );
      div.appendChild(pp);
    }

    result.appendChild(p1);
    result.appendChild(p2);
    result.appendChild(p3);
    result.appendChild(p4);
    result.appendChild(div);
  }
}

initPage();
