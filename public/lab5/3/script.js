async function initPage() {
  let resp = await fetch("./questionAnswerData.json");
  let jsons = await resp.json();

  let result = document.getElementById("result");
  let i = 0

  for (let json of jsons) {
    i++
    let q = document.createElement("b");
    let anss = document.createElement("div")
    let ans1 = document.createElement("div")
    let ans2 = document.createElement("div")
    let ans3 = document.createElement("div")
    let a = document.createElement("input")
    let b = document.createElement("input")
    let c = document.createElement("input")


    a.setAttribute("type","radio")
    b.setAttribute("type","radio")
    c.setAttribute("type","radio")
    a.setAttribute("class","me-1")
    b.setAttribute("class","me-1")
    c.setAttribute("class","me-1")
    a.setAttribute("name","q"+i)
    b.setAttribute("name","q"+i)
    c.setAttribute("name","q"+i)

    q.appendChild(document.createTextNode(i+") "+json.question))


    a.value = json.answers.a
    ans1.appendChild(a)
    ans1.appendChild(document.createTextNode(json.answers.a))
    b.value = json.answers.b
    ans2.appendChild(b)
    ans2.appendChild(document.createTextNode(json.answers.b))
    c.value = json.answers.c
    ans3.appendChild(c)
    ans3.appendChild(document.createTextNode(json.answers.c))

    if(json.answers.correct+"".toLowerCase() == "a")a.checked = true
    else if(json.answers.correct+"".toLowerCase() == "b")b.checked = true
    else if(json.answers.correct+"".toLowerCase() == "c")c.checked = true

    anss.setAttribute("class","ms-3")

    anss.appendChild(ans1)
    anss.appendChild(ans2)
    anss.appendChild(ans3)

    result.appendChild(q);
    result.appendChild(anss);

  }
}

initPage();
