<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap demo</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
</head>

<body class="container">

    <form action="./" method="post" class="mt-2">
        <div class="input-group">
            <label class="input-group-text">กรอกสูตรคูณ</label>
            <?php
            if (strcmp($_SERVER["REQUEST_METHOD"], "POST") == 0) {
                echo '<input value="' . $_POST["mother"] . '" type="number" class="form-control" name="mother" id="mother" />';
            } else {
                echo '<input type="number" class="form-control" name="mother" id="mother" />';
            }
            ?>

            <button type="submit" class="btn btn-dark">แสดงตาราง</button>
        </div>
    </form>

    <?php
    if (strcmp($_SERVER["REQUEST_METHOD"], "POST") == 0) {
        $mother = (int)$_POST["mother"];
        echo "<h3 class=\"mt-3\">ตารางสูตรคูณแม่" . $mother . "</h3>";
        echo "<table style=\"width:fit-content\" class=\"table table-sm mt-3\">";
        for ($i = 1; $i <= 12; $i++) {
            echo "<tr>";
            echo "<td class=\"ps-2 pe-2\">" . $mother . "</td>";
            echo "<td class=\"ps-2 pe-2\">X</td>";
            echo "<td class=\"ps-2 pe-2\">" . $i . "</td>";
            echo "<td class=\"ps-2 pe-2\">=</td>";
            echo "<td class=\"ps-2 pe-2\">" . ($mother * $i) . "</td>";
            echo "</tr>";
        }
        echo "</table>";
    }
    ?>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
</body>

</html>
