<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">

</head>

<body class="container">
    <div class="p-5">
        <form action="./" method="get">

            <div class="input-group">
                <label class="input-group-text">เลือกเดือน</label>
                <select name="month" id="month" class="form-select">
                    <?php
                    $monthInfo = array(
                        "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
                    );
                    for ($i = 1; $i <= 12; $i++) {
                        $thisMonth = 1;
                        if (array_key_exists("month", $_GET) == 1) {
                            $thisMonth = $_GET["month"];
                        }
                        if($thisMonth == $i){
                        echo "<option selected value=\"" . $i . "\">" . $monthInfo[$i - 1] . "</option>";
                        }else{
                        echo "<option value=\"" . $i . "\">" . $monthInfo[$i - 1] . "</option>";

                        }
                    }
                    ?>
                </select>
                <button type="submit" class="btn btn-light">เลือก</button>
            </div>
        </form>
        <table class="table">
            <tr>
                <th>
                    Su
                </th>
                <th>
                    Mo
                </th>
                <th>
                    Tu
                </th>
                <th>
                    We
                </th>
                <th>
                    Th
                </th>
                <th>
                    Fr
                </th>
                <th>
                    Sa
                </th>
            </tr>
            <?php
            $dataInfo = array(
                "Sunday" => 0,
                "Monday" => 1,
                "Tuesday" => 2,
                "Wednesday" => 3,
                "Thursday" => 4,
                "Friday" => 5,
                "Saturday" => 6,
            );

            $thisMonth = 1;
            if (array_key_exists("month", $_GET) == 1) {
                $thisMonth = $_GET["month"];
            }
            $cou = 0;
            for ($i = 1; $i <= 32; $i++) {
                if ($cou == 7) {
                    $cou = 0;
                    echo "<tr>";
                }
                if ($i == 1) {
                    for ($x = 0; $x < $dataInfo[date("l", mktime(0, 0, 0, $thisMonth, $i, 2024))]; $x++) {
                        $cou++;
                        echo "<td></td>";
                    }
                }
                if ((int) date("m", mktime(0, 0, 0, $thisMonth, $i, 2024)) == $thisMonth) {
                    $cou++;
                    echo "<td>" . date("d", mktime(0, 0, 0, $thisMonth, $i, 2024)) . "</td>";
                } else {
                    for ($x = 0; $x < 6 - $dataInfo[date("l", mktime(0, 0, 0, $thisMonth, $i - 1, 2024))]; $x++) {
                        echo "<td></td>";
                    }
                    break;
                }
                if ($cou == 7) {
                    $cou = 0;
                    echo "</tr>";
                }
            }
            ?>
        </table>
    </div>
</body>

</html>
