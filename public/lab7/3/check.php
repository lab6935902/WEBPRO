<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>DD</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">

</head>

<body class="container p-5">
    <form action="./check.php" method="post">

        <h1>Member Registration</h1>
        <label>Name:</label>
        <?php
        if (strlen($_POST["name"]) < 5) {
            echo '<input style="border-color: red;color: red;" name="name" id="name" class="form-control" value="' . $_POST["name"] . '" />';
        } else {
            echo '<input style="border-color: green;color: green;" name="name" id="name" class="form-control" value="' . $_POST["name"] . '"  />';
        }
        ?>
        <label>Address:</label>

        <?php
        if (strlen($_POST["addr"]) < 5) {
            echo '<textarea style="border-color: red;color: red;" name="addr" id="addr" class="form-control">' . $_POST["addr"] . '</textarea>';
        } else {
            echo  '<textarea style="border-color: green;color: green;" name="addr" id="addr" class="form-control">' . $_POST["addr"] . '</textarea>';
        }
        ?>
        <label>Age:</label>
        <?php
        if (strlen($_POST["age"]) <= 0) {
            echo '<input type="number" style="border-color: red;color: red;" type="number" name="age" id="age" style="width:200px" class="form-control" value="'.$_POST["age"].'" />';
        } else {
            echo '<input type="number" style="border-color: green;color: green;" type="number" name="age" id="age" style="width:200px" class="form-control" value="'.$_POST["age"].'" />';
        }

        ?>


        <label>Profession</label>
        <?php
        if (strlen($_POST["profession"]) < 5) {
            echo '<input  style="border-color: red;color: red;" name="profession" id="profession" class="form-control" value="' . $_POST["profession"] . '"  />';
        } else {
            echo '<input style="border-color: green;color: green;" name="profession" id="profession" class="form-control" value="' . $_POST["profession"] . '"  />';
        }
        ?>
        <label>Residential Status</label>
        <br />


        <?php
        if (strcmp($_POST["rstatus"], "resident") == 0) {
            echo '<input value="resident" class="form-check-input" type="radio" name="rstatus" id="rstatus1" checked>
            <label class="form-check-label" for="rstatus1">Resident</label> 
            <input class="form-check-input" type="radio" name="rstatus" id="rstatus2">
            <label value="non-resident" class="form-check-label" for="rstatus2">
                Non-Resident
            </label>
            ';
        } else {
            echo '<input value="resident" class="form-check-input" type="radio" name="rstatus" id="rstatus1" >
            <label class="form-check-label" for="rstatus1">Resident</label> 
            <input class="form-check-input" type="radio" name="rstatus" id="rstatus2" checked>
            <label value="non-resident" class="form-check-label" for="rstatus2">
                Non-Resident
            </label>
            ';
        }
        ?>




    </form>
</body>

</html>
