<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>wow</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
</head>

<body class="container p-5">
    <form action="./check.php" method="post">
        <h1>Member Registration</h1>
        <label>Name:</label>
        <input name="name" id="name" class="form-control" />
        <label>Address:</label>
        <textarea name="addr" id="addr" class="form-control"></textarea>
        <label>Age:</label>
        <input type="number" name="age" id="age" style="width:200px" class="form-control" />
        <label>Profession</label>
        <input name="profession" id="profession" class="form-control" />
        <label>Residential Status</label>
        <br />

        <input value="resident" class="form-check-input" type="radio" name="rstatus" id="rstatus1" checked>
        <label class="form-check-label" for="rstatus1">
            Resident
        </label>
        <input class="form-check-input" type="radio" name="rstatus" id="rstatus2">
        <label value="non-resident" class="form-check-label" for="rstatus2">
            Non-Resident
        </label>
        <br />
        <br />


        <button type="submit" class="btn btn-primary">Submit</button>

    </form>
</body>

</html>
