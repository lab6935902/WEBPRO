<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>wow</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <style>
        .img-gal {
            column-count: 4;
        }

        .img-gal>img {
            padding: 5px;
        }
    </style>
</head>

<body class="container p-5">
    <div class="img-gal">
        <?php

        for ($i = 1; $i <= 30; $i++) {
            $randd = rand(100, 900);
            echo "<img width='100%' src='https://picsum.photos/300/" . $randd . "' />";
        }
        ?>
    </div>
</body>

</html>
