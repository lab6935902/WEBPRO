<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/water.css@2/out/dark.css">
    <style>
        .flex{
            display:flex;
            align-items: center;
            gap:5px;
        }
    </style>
</head>
<body>
    <?php
    $servername = "localhost";
    $username = "root"; //ตามที่กำหนดให้
    $password = ""; //ตามที่กำหนดให้
    $dbname = "s242f";    //ตามที่กำหนดให้
    // Create connection
    $conn = mysqli_connect($servername, $username, $password, $dbname);
    // Check connection
    if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
    }
    //echo "Connected successfully";

    if(key_exists("offset",$_GET)){
        $offset = $_GET["offset"];

    }else{
        $offset = "0";
    }

    $sql = "SELECT * FROM `course` LIMIT 1 OFFSET ".mysqli_real_escape_string($conn,$offset).";";
    $result = mysqli_query($conn, $sql);

    // close connection
    mysqli_close($conn);
    ?>

  
    <form method="get" action="./">
        <h1>Course Data</h1>
    <?php
        if (mysqli_num_rows($result) > 0) {
          // output data of each row
          while($row = mysqli_fetch_assoc($result)) {
            echo "<div class='flex'>";
            echo "<label>ID :</label>";
            echo "<input value='" . $row["course_id"]. "'/> " ;
            echo "</div>";

            echo "<div class='flex'>";
            echo "<label>Title :</label>";
            echo "<input value='" . $row["title"]. "'/> ";
            echo "</div>";

            echo "<div class='flex'>";
            echo "<label>Dept :</label>";
            echo "<input value='" . $row["dept_name"]. "'/> ";
            echo "</div>";

            echo "<div class='flex'>";
            echo "<label>Credits :</label>";
            echo "<input value='" . $row["credits"]. "'/> ";
            echo "</div>";

            echo "<br/>";
            echo "<br/>";
            echo "<br/>";


            
          }
        } else {
          echo "<blockquote>Not found</blockquote>";
        }
        echo "<div class='flex'>";
        echo "<label>Enter a record number </label>";
        echo "<input name='offset' value='" . $offset. "'/> ";
        
    ?>
        <button type="submit">Display</button>
    </div>
    </form>
</body>
</html>