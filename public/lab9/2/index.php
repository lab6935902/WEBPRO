<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/water.css@2/out/dark.css">
    <style>
        .flex {
            display: flex;
            align-items: center;
            gap: 5px;
        }
    </style>
</head>

<body>
    <?php
    $servername = "localhost";
    $username = "S242F"; //ตามที่กำหนดให้
    $password = "XP34067"; //ตามที่กำหนดให้
    $dbname = "s242f";    //ตามที่กำหนดให้
    // Create connection
    $conn = mysqli_connect($servername, $username, $password, $dbname);
    // Check connection
    if (!$conn) {
        die("Connection failed: " . mysqli_connect_error());
    }
    //echo "Connected successfully";
    $sql = "SELECT * FROM `instructor` ;";
    $result = mysqli_query($conn, $sql);

    $sql = "SELECT dept_name FROM `department`;";
    $resultOfDepartment = mysqli_query($conn, $sql);


    // close connection
    mysqli_close($conn);
    ?>


    <form method="post" action="./">
        <h1>Instructor Data</h1>

        <div class='flex'>
            <label>ID :</label>
            <input id="id" name='id' style="display: none;" />
            <input id="id2" name="id2" />
        </div>

        <div class='flex'>
            <label>Name :</label>
            <input id="name" name='name' value='' />
        </div>

        <div class='flex'>
            <label>Dept :</label>
            <select id='dept_name' name='dept_name' value=''>
                <option value="" selected disabled>Select department</option>
                <?php
                if (mysqli_num_rows($resultOfDepartment) > 0) {
                    // output data of each row
                    while ($row = mysqli_fetch_assoc($resultOfDepartment)) {
                        echo "<option value='" . $row["dept_name"] . "'>" . $row["dept_name"] . "</option>";
                    }
                } else {
                    echo "<option>Not found any department</option>";
                }
                ?>
            </select>
        </div>

        <div class='flex'>
            <label>Salary :</label>
            <input id="salary" name='salary' value='' />
        </div>

        <br />

        <div class='flex'>
            <button onclick="updateHandler()" type="submit">UPDATE</button>
            <button onclick="deleteHandler()" type="submit">DELETE</button>
        </div>
        <br />

        <table>
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Dept name </th>
                    <th>Salary</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if (mysqli_num_rows($result) > 0) {
                    // output data of each row
                    while ($row = mysqli_fetch_assoc($result)) {
                        echo "<tr>";
                        $params = "'" . $row["ID"] . "','" . $row["name"] . "','" . $row["dept_name"] . "','" . $row["salary"] . "'";
                        echo "<td><a onclick=\"changeForm(" . $params . ")\" >" . $row["ID"] . "</a></td>";
                        echo "<td>" . $row["name"] . "</td>";
                        echo "<td>" . $row["dept_name"] . "</td>";
                        echo "<td>" . $row["salary"] . "</td>";
                        echo "</tr>";
                    }
                } else {
                    echo "<blockquote>Not found</blockquote>";
                }

                ?>
            </tbody>
        </table>
    </form>
    <script>
        function changeForm(id, name, dept, salary) {
            document.getElementById("id").value = id;
            document.getElementById("id2").value = id;
            document.getElementById("name").value = name;
            document.getElementById("dept_name").value = dept;
            document.getElementById("salary").value = salary;
        }

        function updateHandler() {
            document.forms[0].action = "./update.php"
            document.forms[0].submit()
        }

        function deleteHandler() {
            document.forms[0].action = "./delete.php"
            document.forms[0].submit()
        }
    </script>
</body>

</html>
